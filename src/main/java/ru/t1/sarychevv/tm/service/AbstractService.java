package ru.t1.sarychevv.tm.service;

import ru.t1.sarychevv.tm.api.repository.IRepository;
import ru.t1.sarychevv.tm.api.service.IService;
import ru.t1.sarychevv.tm.enumerated.Sort;
import ru.t1.sarychevv.tm.enumerated.Status;
import ru.t1.sarychevv.tm.exception.entity.ModelNotFoundException;
import ru.t1.sarychevv.tm.exception.entity.ProjectNotFoundException;
import ru.t1.sarychevv.tm.exception.entity.TaskNotFoundException;
import ru.t1.sarychevv.tm.exception.field.*;
import ru.t1.sarychevv.tm.model.AbstractModel;

import java.util.Comparator;
import java.util.List;

public abstract class AbstractService<M extends AbstractModel, R extends IRepository<M>> implements IService<M> {

    protected final R repository;

    protected AbstractService(R repository) {
        this.repository = repository;
    }

    @Override
    public void add(final M model) {
        if (model == null) throw new ModelEmptyException();
        repository.add(model);
    }

    @Override
    public List<M> findAll() {
        return repository.findAll();
    }

    @Override
    public List<M> findAll(final Comparator comparator) {
        if (comparator == null) return findAll();
        return repository.findAll(comparator);
    }

    @Override
    public List<M> findAll(final Sort sort) {
        if (sort == null) return findAll();
        return findAll(sort);
    }

    @Override
    public M findOneById(final String id) {
        if (id == null || id.isEmpty()) throw new ModelIdEmptyException();
        return repository.findOneById(id);
    }

    @Override
    public M findOneByIndex(final Integer index) {
        if (index == null || index < 0) throw new IndexIncorrectException();
        return repository.findOneByIndex(index);
    }

    @Override
    public M updateById(final String id, final String name, final String description) {
        if (id == null || id.isEmpty()) throw new TaskIdEmptyException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        final M model = findOneById(id);
        if (model == null) throw new ModelNotFoundException();
        model.setName(name);
        model.setDescription(description);
        return model;
    }

    @Override
    public M updateByIndex(final Integer index, final String name, final String description) {
        if (index == null || index < 0) throw new IndexIncorrectException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        final M model = findOneByIndex(index);
        if (model == null) throw new ModelNotFoundException();
        model.setName(name);
        model.setDescription(description);
        return model;
    }

    @Override
    public void clear() {
        repository.clear();
    }

    @Override
    public M remove(final M model) {
        if (model == null) throw new ModelEmptyException();
        repository.remove(model);
        return model;
    }

    @Override
    public M removeById(final String id) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        return repository.removeById(id);
    }

    @Override
    public M removeByIndex(final Integer index) {
        if (index == null || index < 0) throw new IndexIncorrectException();
        return repository.removeByIndex(index);
    }

    @Override
    public Integer getSize() {
        return repository.getSize();
    }

    @Override
    public Boolean existsById(final String id) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        return repository.existsById(id);
    }

    @Override
    public M changeStatusById(final String id, final Status status) {
        if (id == null || id.isEmpty()) throw new ProjectIdEmptyException();
        final M model = findOneById(id);
        if (model == null) throw new ProjectNotFoundException();
        model.setStatus(status);
        return model;
    }

    @Override
    public M changeStatusByIndex(final Integer index, final Status status) {
        if (index == null || index < 0 || index >= repository.getSize()) throw new IndexIncorrectException();
        final M model = findOneByIndex(index);
        if (model == null) throw new TaskNotFoundException();
        model.setStatus(status);
        return model;
    }
}
