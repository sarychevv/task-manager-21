package ru.t1.sarychevv.tm.command;

import ru.t1.sarychevv.tm.api.model.IAuthService;
import ru.t1.sarychevv.tm.api.model.ICommand;
import ru.t1.sarychevv.tm.api.service.IServiceLocator;
import ru.t1.sarychevv.tm.enumerated.Role;

public abstract class AbstractCommand implements ICommand {

    protected IServiceLocator serviceLocator;

    public IServiceLocator getServiceLocator() {
        return serviceLocator;
    }

    public void setServiceLocator(final IServiceLocator serviceLocator) {
        this.serviceLocator = serviceLocator;
    }

    public abstract Role[] getRoles();

    public String getUserId() {
        final IAuthService authService = getServiceLocator().getAuthService();
        return authService.getUserId();
    }

    @Override
    public String toString() {
        final String name = getName();
        final String argument = getArgument();
        final String description = getDescription();
        String result = "";
        if (name != null && !name.isEmpty()) result += name;
        if (argument != null && !argument.isEmpty()) result += ", " + argument;
        if (description != null && !description.isEmpty()) result += ": " + description;
        return result;
    }

}
