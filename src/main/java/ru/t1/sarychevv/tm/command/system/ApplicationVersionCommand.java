package ru.t1.sarychevv.tm.command.system;

public class ApplicationVersionCommand extends AbstractSystemCommand {

    @Override
    public void execute() {
        System.out.println("[VERSION]");
        System.out.println("1.21.0");
    }

    @Override
    public String getName() {
        return "version";
    }

    @Override
    public String getArgument() {
        return "-v";
    }

    @Override
    public String getDescription() {
        return "Show program version.";
    }

}
