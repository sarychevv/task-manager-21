package ru.t1.sarychevv.tm.command.user;

import ru.t1.sarychevv.tm.enumerated.Role;
import ru.t1.sarychevv.tm.util.TerminalUtil;

public class UserUpdateProfileCommand extends AbstractUserCommand {

    @Override
    public String getDescription() {
        return "Update user profile.";
    }

    @Override
    public String getName() {
        return "update-user-profile";
    }

    @Override
    public void execute() {
        final String userId = serviceLocator.getAuthService().getUserId();
        System.out.println("[USER UPDATE PROFILE]");
        System.out.println("FIRST NAME:");
        final String firstName = TerminalUtil.nextLine();
        System.out.println("LAST NAME:");
        final String lastName = TerminalUtil.nextLine();
        System.out.println("MIDDLE NAME:");
        final String middleName = TerminalUtil.nextLine();
        serviceLocator.getUserService().updateUser(userId, firstName, lastName, middleName);
    }

    @Override
    public Role[] getRoles() {
        return Role.values();
    }

}
